﻿namespace QuizDataEntry {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.cbQuizType = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.tbQuestion = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.tbAnswerA = new System.Windows.Forms.TextBox();
			this.tbAnswerB = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.tbAnswerC = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.tbAnswerD = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.cbCorrectAnswer = new System.Windows.Forms.ComboBox();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.btnSaveToFile = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.SuspendLayout();
			// 
			// cbQuizType
			// 
			this.cbQuizType.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
			this.cbQuizType.FormattingEnabled = true;
			this.cbQuizType.Location = new System.Drawing.Point(76, 6);
			this.cbQuizType.Name = "cbQuizType";
			this.cbQuizType.Size = new System.Drawing.Size(143, 21);
			this.cbQuizType.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 9);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "Quiz Type:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(248, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Note: Try not to enter long questions and answers. ";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 71);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(52, 13);
			this.label4.TabIndex = 5;
			this.label4.Text = "Question:";
			// 
			// tbQuestion
			// 
			this.tbQuestion.Location = new System.Drawing.Point(70, 68);
			this.tbQuestion.Name = "tbQuestion";
			this.tbQuestion.Size = new System.Drawing.Size(471, 20);
			this.tbQuestion.TabIndex = 2;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 104);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(129, 13);
			this.label5.TabIndex = 6;
			this.label5.Text = "Multiple choose selection:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 128);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(17, 13);
			this.label6.TabIndex = 7;
			this.label6.Text = "A:";
			// 
			// tbAnswerA
			// 
			this.tbAnswerA.Location = new System.Drawing.Point(70, 125);
			this.tbAnswerA.Name = "tbAnswerA";
			this.tbAnswerA.Size = new System.Drawing.Size(471, 20);
			this.tbAnswerA.TabIndex = 3;
			// 
			// tbAnswerB
			// 
			this.tbAnswerB.Location = new System.Drawing.Point(70, 151);
			this.tbAnswerB.Name = "tbAnswerB";
			this.tbAnswerB.Size = new System.Drawing.Size(471, 20);
			this.tbAnswerB.TabIndex = 4;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 154);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(17, 13);
			this.label7.TabIndex = 9;
			this.label7.Text = "B:";
			// 
			// tbAnswerC
			// 
			this.tbAnswerC.Location = new System.Drawing.Point(70, 177);
			this.tbAnswerC.Name = "tbAnswerC";
			this.tbAnswerC.Size = new System.Drawing.Size(471, 20);
			this.tbAnswerC.TabIndex = 5;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 180);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(17, 13);
			this.label8.TabIndex = 11;
			this.label8.Text = "C:";
			// 
			// tbAnswerD
			// 
			this.tbAnswerD.Location = new System.Drawing.Point(70, 203);
			this.tbAnswerD.Name = "tbAnswerD";
			this.tbAnswerD.Size = new System.Drawing.Size(471, 20);
			this.tbAnswerD.TabIndex = 6;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(12, 206);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(18, 13);
			this.label9.TabIndex = 13;
			this.label9.Text = "D:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(12, 237);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(82, 13);
			this.label10.TabIndex = 14;
			this.label10.Text = "Correct Answer:";
			// 
			// cbCorrectAnswer
			// 
			this.cbCorrectAnswer.FormattingEnabled = true;
			this.cbCorrectAnswer.Location = new System.Drawing.Point(100, 234);
			this.cbCorrectAnswer.Name = "cbCorrectAnswer";
			this.cbCorrectAnswer.Size = new System.Drawing.Size(49, 21);
			this.cbCorrectAnswer.TabIndex = 7;
			// 
			// btnSubmit
			// 
			this.btnSubmit.Location = new System.Drawing.Point(253, 232);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(75, 23);
			this.btnSubmit.TabIndex = 8;
			this.btnSubmit.Text = "Submit";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// btnSaveToFile
			// 
			this.btnSaveToFile.Location = new System.Drawing.Point(466, 232);
			this.btnSaveToFile.Name = "btnSaveToFile";
			this.btnSaveToFile.Size = new System.Drawing.Size(75, 23);
			this.btnSaveToFile.TabIndex = 9;
			this.btnSaveToFile.Text = "Save To File";
			this.btnSaveToFile.UseVisualStyleBackColor = true;
			this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(565, 268);
			this.Controls.Add(this.btnSaveToFile);
			this.Controls.Add(this.btnSubmit);
			this.Controls.Add(this.cbCorrectAnswer);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.tbAnswerD);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.tbAnswerC);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.tbAnswerB);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.tbAnswerA);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.tbQuestion);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.cbQuizType);
			this.Name = "Form1";
			this.Text = "Quiz Data Entry";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
		private System.Windows.Forms.ComboBox cbQuizType;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox tbQuestion;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tbAnswerA;
		private System.Windows.Forms.TextBox tbAnswerB;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox tbAnswerC;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox tbAnswerD;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox cbCorrectAnswer;
		private System.Windows.Forms.Button btnSubmit;
		private System.Windows.Forms.Button btnSaveToFile;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
	}
}

