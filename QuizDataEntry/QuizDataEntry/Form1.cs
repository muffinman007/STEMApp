﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuizDataEntry {
	public partial class Form1 : Form {

		string bioFileName				= "biologyQuiz.xml";
		string chemFileName				= "chemistryQuiz.xml";

		// xml attribute names
		string bioParentNode			= "BioQuiz";
		string chemParentNode			= "ChemQuiz";
		string firstChildNode			= "Question";
		string subChildNode				= "Choose";

		string questionAttributeName	= "question";
		string answerAttributeName		= "answer";

		string attributeChooseA			= "a";
		string attributeChooseB			= "b";
		string attributeChooseC			= "c";
		string attributeChooseD			= "d";

		string xmlDelimitor				= "$";

		string biologyStr				= "Biology";
		string chemistryStr				= "Chemistry";
		string aStr						= "A";
		string bStr						= "B";
		string cStr						= "C";
		string dStr						= "D";

		string quizTypeSelected;

		StringBuilder sb;

		bool isFirstData				= true;

		public Form1() {
			InitializeComponent();

			sb = new StringBuilder();

			cbQuizType.Items.Add(biologyStr);
			cbQuizType.Items.Add(chemistryStr);
			cbQuizType.Text = "";

			cbCorrectAnswer.Items.Add(aStr);
			cbCorrectAnswer.Items.Add(bStr);
			cbCorrectAnswer.Items.Add(cStr);
			cbCorrectAnswer.Items.Add(dStr);
			cbCorrectAnswer.Text = "";
		}

		private void btnSubmit_Click(object sender, EventArgs e) {
			if(String.IsNullOrWhiteSpace(cbQuizType.Text) ||
			   String.IsNullOrWhiteSpace(tbQuestion.Text) ||
			   String.IsNullOrWhiteSpace(tbAnswerA.Text)  ||
			   String.IsNullOrWhiteSpace(tbAnswerB.Text)  ||
			   String.IsNullOrWhiteSpace(tbAnswerC.Text)  ||
			   String.IsNullOrWhiteSpace(tbAnswerD.Text)  ||
			   String.IsNullOrWhiteSpace(cbCorrectAnswer.Text) ) {

				MessageBox.Show("ERROR: Make sure all fields are filled out.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}			

			if(isFirstData) {
				quizTypeSelected = cbQuizType.Text;

				// first parent node of the xml document
				if(cbQuizType.Text.Equals(biologyStr)) {
					sb.Append("<" + bioParentNode + ">" + Environment.NewLine);
				}
				else if(cbQuizType.Text.Equals(chemistryStr)) {
					sb.Append("<" + chemParentNode + ">" + Environment.NewLine);
				}

				isFirstData = false;
			}
			
			// creating node and sub node
			// Question node
			string[] temp = tbQuestion.Text.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries);
			sb.Append("<" + firstChildNode + " " + questionAttributeName + "=\"");
			sb.Append(temp[0]);
			for(int i = 1; i < temp.Length; ++i) {
				sb.Append(xmlDelimitor + temp[i]);
			}
			sb.Append("\" " + answerAttributeName + "=\"");
			if(cbCorrectAnswer.Text.Equals(aStr)) {
				sb.Append("0\">" + Environment.NewLine);
			}
			else if(cbCorrectAnswer.Text.Equals(bStr)){
				sb.Append("1\">" + Environment.NewLine);
			}
			else if(cbCorrectAnswer.Text.Equals(cStr)){
				sb.Append("2\">" + Environment.NewLine);
			}
			else if(cbCorrectAnswer.Text.Equals(dStr)){
				sb.Append("3\">" + Environment.NewLine);
			}

			// sub Choose node
			sb.Append("<" + subChildNode + " ");
			extractChoose(attributeChooseA ,	tbAnswerA.Text.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries));
			extractChoose(attributeChooseB,		tbAnswerB.Text.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries));
			extractChoose(attributeChooseC,		tbAnswerC.Text.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries));
			extractChoose(attributeChooseD,		tbAnswerD.Text.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries));
			sb.Append("/>" + Environment.NewLine);
			sb.Append("</" + firstChildNode + ">" + Environment.NewLine);

			// clear question and answer
			tbQuestion.Clear();
			tbAnswerA.Clear();
			tbAnswerB.Clear();
			tbAnswerC.Clear();
			tbAnswerD.Clear();
			cbCorrectAnswer.Text = "";

			cbQuizType.Text = quizTypeSelected;
		}


		private void extractChoose(string attrbute, string[] data) {
			sb.Append(attrbute + "=\"");
			if(data.Length == 1) {
				sb.Append(data[0] + "\" ");
			}
			else {
				sb.Append(data[0]);
				for(int i = 1; i < data.Length; ++i) {
					sb.Append(xmlDelimitor + data[i]);
				}
				sb.Append("\" ");
			}
		}

		private void btnSaveToFile_Click(object sender, EventArgs e) {
			if(sb.Length == 0) {
				MessageBox.Show("Nothing to save.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}

			if(quizTypeSelected.Equals(biologyStr)) {
				sb.Append("</" + bioParentNode + ">");
			}
			else if(quizTypeSelected.Equals(chemistryStr)) {
				sb.Append("</" + chemParentNode + ">");
			}


			// show file save dialog
			saveFileDialog1.FileName = quizTypeSelected.Equals(biologyStr) ? bioFileName : chemFileName;			
			saveFileDialog1.ShowDialog();

			// clear question and answer
			tbQuestion.Clear();
			tbAnswerA.Clear();
			tbAnswerB.Clear();
			tbAnswerC.Clear();
			tbAnswerD.Clear();
			cbCorrectAnswer.Text = "";
			cbQuizType.Text = "";
			isFirstData = true;
			quizTypeSelected = "";
		}

		private void saveFileDialog1_FileOk(object sender, CancelEventArgs e) {
			// save to file.
			using(System.IO.StreamWriter sw = new System.IO.StreamWriter(saveFileDialog1.FileName)){
				sw.Write(sb.ToString());				
			}
		}
	}
}
