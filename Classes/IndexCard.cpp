#include"IndexCard.h"
#include<algorithm>
#include<random>
#include<vector>
#include<chrono>


IndexCard* IndexCard::create(){
	IndexCard* self = new IndexCard();

	if(self && self->initWithFile("res/indexCard.png")){
		self->autorelease();
		return self;
	}
	CC_SAFE_DELETE(self);
	return nullptr;
}


void IndexCard::DataFeed(GlobalConfig::DataClass dataClass, xml_node data){
	this->setTexture("res/indexCard.png");

	// set data
	if(dataClass == GlobalConfig::DataClass::PERIODIC_TABLE){
		if(chemSymbol == nullptr){
			chemSymbol	 = Label::createWithTTF("", "fonts/Helvetica.ttf", 300);
			chemAtomNum	 = Label::createWithTTF("", "fonts/Helvetica.ttf", 130);
			chemName	 = Label::createWithTTF("", "fonts/Helvetica.ttf", 130);
			chemAtomMass = Label::createWithTTF("", "fonts/Helvetica.ttf", 130);	

			chemSymbol->setTextColor(Color4B::BLACK);
			chemAtomNum->setTextColor(Color4B::BLACK);	
			chemName->setTextColor(Color4B::BLACK);	
			chemAtomMass->setTextColor(Color4B::BLACK); 

			// set up location
			//auto size = Director::getInstance()->getVisibleSize();
			chemSymbol->setPosition(this->getContentSize() / 2.0f);
			chemAtomNum->setPosition(chemSymbol->getBoundingBox().getMinX() - 260.0f, chemSymbol->getBoundingBox().getMaxY() + 220.0f);
			chemName->setPosition(chemSymbol->getBoundingBox().getMidX(), chemSymbol->getBoundingBox().getMinY() - 160.0f);
			chemAtomMass->setPosition(chemName->getBoundingBox().getMidX(), chemName->getBoundingBox().getMinY() - 140.0f);

			this->addChild(chemSymbol);
			this->addChild(chemAtomNum);
			this->addChild(chemName);
			this->addChild(chemAtomMass);
		}		

		// extracting data
		chemSymbol->setString(data.attribute("symbol").value());
		chemAtomNum->setString(data.attribute("atomicNumber").value());
		chemName->setString(data.attribute("name").value());
		chemAtomMass->setString(data.attribute("atomicMass").value());		

		if(RandomHelper::random_int(0, 1) == 0){
			chemSymbol->setVisible(true);
			chemAtomNum->setVisible(true);	
			chemName->setVisible(true);	
			chemAtomMass->setVisible(true);

			// cover up two of the label
			std::vector<int> items;
			items.push_back(0);
			items.push_back(1);
			items.push_back(2);
			items.push_back(3);
			std::shuffle(std::begin(items), std::end(items), std::default_random_engine(chrono::system_clock::now().time_since_epoch().count()));

			for(int i = 0; i < 2; ++i){
				switch(items.at(i)){
					case 0:
						chemSymbol->setVisible(false);
						break;
					case 1:
						chemAtomNum->setVisible(false);
						break;
					case 2:
						chemName->setVisible(false);
						break;
					case 3:
						chemAtomMass->setVisible(false);
						break;
					
				}
			}
		}
		else{
			// cover up three of the label
			switch(RandomHelper::random_int(0, 3)){
				case 0:
					chemSymbol->setVisible(true);
					chemAtomNum->setVisible(false);	
					chemName->setVisible(false);	
					chemAtomMass->setVisible(false);
					break;
				case 1:
					chemSymbol->setVisible(false);		
					chemAtomNum->setVisible(true);		
					chemName->setVisible(false);		
					chemAtomMass->setVisible(false);
					break;
				case 2:
					chemSymbol->setVisible(false);		
					chemAtomNum->setVisible(false);		
					chemName->setVisible(true);		
					chemAtomMass->setVisible(false);
					break;
				case 3:
					chemSymbol->setVisible(false);		
					chemAtomNum->setVisible(false);		
					chemName->setVisible(false);		
					chemAtomMass->setVisible(true);
					break;
			}
		}

		chemSymbolVisible	= chemSymbol->isVisible();
		chemAtomNumVisible	= chemAtomNum->isVisible();
		chemNameVisible		= chemName->isVisible();
		chemAtomMassVisible	= chemAtomMass->isVisible();		
	}// end periodic table

	isAnswerShown = false;
}



void IndexCard::flip(){
	this->runAction(Sequence::create(
		RotateBy::create(0.2f, 90.0f, 0.0f),
		CallFunc::create([&](){
			if(isAnswerShown){ 
				// the answer is shown, show the question
				this->setTexture("res/indexCard.png");
				chemSymbol->setVisible(chemSymbolVisible);		
				chemAtomNum->setVisible(chemAtomNumVisible);		
				chemName->setVisible(chemNameVisible);		
				chemAtomMass->setVisible(chemAtomMassVisible);
				isAnswerShown = false;
			}
			else{
				// the question is shown, reveal the answer please
				this->setTexture("res/indexCardFlipped.png");
				chemSymbol->setVisible(true);		
				chemAtomNum->setVisible(true);		
				chemName->setVisible(true);		
				chemAtomMass->setVisible(true);
				isAnswerShown = true;
			}			
		}),
		RotateBy::create(0.2f, -90.0f, 0.0f),
		nullptr
	));	
}


