#ifndef __QUIZ_H__
#define __QUIZ_H__

#include"cocos2d.h"
#include"GlobalConfig.h"
#include"pugixml.hpp"
#include<vector>
#include"ui\CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace pugi;
using namespace std;

enum class AnswerSelection{ A, B, C, D };

class Quiz : public Layer{
	public:
		static Scene* createScene();
		virtual bool init();
		CREATE_FUNC(Quiz);

		Button* homeBtn = nullptr;

		void goToMainMenu();
		void nextQuestion();
		void checkAnswer(AnswerSelection answerSelected);		

		virtual bool onTouchBegin(Touch* touch, Event* event);
		virtual void onTouchMoved(Touch* touch, Event* event);
		virtual void onTouchEnded(Touch* touch, Event* event);
		virtual void onTouchCancelled(Touch* touch, Event* event);

	private:

		vector<xml_node> dataVec;
		pugi::xml_document xmlBioDoc;
		pugi::xml_document xmlChemDoc;
		void extractXmlBioDoc();
		void extractXmlChemDoc();

		int currentIndex = 0;

		void loadData();
		void createUI();
		string getString(string value);

		Label* question;
		Button* answerA;
		Button* answerB;
		Button* answerC;
		Button* answerD;

		Label* aLabel;
		Label* bLabel;
		Label* cLabel;
		Label* dLabel;

		int correctAnswer;
		int userAnswer;

		static float answerFontSize;
		static float questionFontSize;
		static float answerLabelXOffset;

		Vec2 initTouchPosition;
};

#endif // !__QUIZ_H__

