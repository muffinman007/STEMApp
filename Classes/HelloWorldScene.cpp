#include "HelloWorldScene.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();  

    // add a "close" icon to exit the progress. it's an autorelease object
 //   auto closeItem = MenuItemImage::create(
 //                                          "CloseNormal.png",
 //                                          "CloseSelected.png",
 //                                          CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
 //   
	//closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
 //                               origin.y + closeItem->getContentSize().height/2));

 //   // create menu, it's an autorelease object
 //   auto menu = Menu::create(closeItem, NULL);
 //   menu->setPosition(Vec2::ZERO);
 //   this->addChild(menu, 1);


	// Rotate To is left most , rotate by is right most
    
    auto label = Label::createWithTTF("RotateTo", "fonts/Marker Felt.ttf", 30);
    //label->setPosition(Vec2(origin.x + visibleSize.width / 4.0f, origin.y + visibleSize.height - label->getContentSize().height));

    //this->addChild(label);

	auto l2 = Label::createWithTTF("RotatBy", "fonts/Marker Felt.ttf", 30);
	//l2->setPosition(Vec2(origin.x + visibleSize.width * (3.0f/4.0f), origin.y + visibleSize.height - l2->getContentSize().height));

	auto labelBack = Label::createWithTTF("RotateToBack", "fonts/Marker Felt.ttf", 30);
    //label->setPosition(Vec2(origin.x + visibleSize.width / 4.0f, origin.y + label->getContentSize().height * 2));

    //this->addChild(label);

	auto l2Back = Label::createWithTTF("RotatByBack", "fonts/Marker Felt.ttf", 30);
	//l2->setPosition(Vec2(origin.x + visibleSize.width * (3.0f/4.0f), origin.y + l2->getContentSize().height * 2));
	//this->addChild(l2);


#pragma region Sprite
	srt = Sprite::create("res/AUG20.jpg");
	srb = Sprite::create("res/AUG20.jpg");

	srt->setPosition(Vec2(origin.x + visibleSize.width / 4.0f, origin.y + visibleSize.height / 2.0f));
	srb->setPosition(Vec2(origin.x + visibleSize.width * (3.0f/4.0f), origin.y + visibleSize.height / 2.0f));
	this->addChild(srt);
	this->addChild(srb);
#pragma endregion
	
	
	// menu buttons
	auto srtm = MenuItemLabel::create(label, CC_CALLBACK_0(HelloWorld::srtRotateTo, this));
	srtm->setPosition(Vec2(origin.x + visibleSize.width / 4.0f,
                            origin.y + visibleSize.height - label->getContentSize().height));

	auto srbm = MenuItemLabel::create(l2, CC_CALLBACK_0(HelloWorld::srbRotateBy, this));
	srbm->setPosition(
		Vec2(
			origin.x + visibleSize.width * (3.0f/4.0f), 
			origin.y + visibleSize.height - l2->getContentSize().height
		)
	);

	auto srtbm = MenuItemLabel::create(labelBack, CC_CALLBACK_0(HelloWorld::srtRotateToBack, this));
	srtbm->setPosition(Vec2(origin.x + visibleSize.width / 4.0f,
                            origin.y + label->getContentSize().height * 2));

	auto srbbm = MenuItemLabel::create(l2Back, CC_CALLBACK_0(HelloWorld::srbRotateByBack, this));
	srbbm->setPosition(
		Vec2(
			origin.x + visibleSize.width * (3.0f/4.0f), 
			origin.y + l2->getContentSize().height * 2
		)
	);

	auto* menu = Menu::create(srtm, srbm, srtbm, srbbm, nullptr);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);

    //// add "HelloWorld" splash screen"
    //auto sprite = Sprite::create("HelloWorld.png");

    //// position the sprite on the center of the screen
    //sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    //// add the sprite as a child to this layer
    //this->addChild(sprite, 0);
    
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
}

/////////////////////////////////////////
//// rotate by 180 degree
/////////////////////////////////////////
void HelloWorld::srbRotateBy(){
	// RotateBy(time, delta angle x, delta angle y)
	srb->runAction(RotateBy::create(2.0f, dx, dy));
}

void HelloWorld::srbRotateByBack(){
	srb->runAction(RotateBy::create(2.0f, -dx, -dy));
}

void HelloWorld::srtRotateTo(){
	srt->runAction(RotateTo::create(2.0f, dx, dy));
}

void HelloWorld::srtRotateToBack(){
	srt->runAction(RotateBy::create(2.0f, -dx, -dy));
}

