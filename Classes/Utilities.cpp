#include"Utilities.h"
#include"MainMenu.h"

Scene* Utilities::createScene(){
	auto scene = Scene::create();
	auto layer = Utilities::create();
	scene->addChild(layer);

	return scene;
}

bool Utilities::init(){
	if(!Layer::init()) return false;

	// home button
	// UI
	homeBtn = Button::create("res/gui/home.png", "res/gui/homeSelect.png");
	homeBtn->setOpacity(0);
	homeBtn->setPosition(Vec2(Director::getInstance()->getVisibleSize().width * 0.9f, Director::getInstance()->getVisibleSize().height * 0.9f));
	homeBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Utilities::goToMainMenu, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	homeBtn->setSwallowTouches(true);
	this->addChild(homeBtn);

	homeBtn->runAction(FadeIn::create(2.0f));

	return true;
}

void Utilities::goToMainMenu(){
	this->_eventDispatcher->removeAllEventListeners();

	// clean up
	//dataVec.clear();

	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.7f, MainMenu::createScene()));
}