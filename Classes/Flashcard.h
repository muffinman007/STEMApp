#ifndef __FLASHCARD_H__
#define __FLASHCARD_H__

#include"cocos2d.h"
#include"ui\CocosGUI.h"
#include"GlobalConfig.h"
#include"IndexCard.h"
#include<vector>
#include"pugixml.hpp"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace pugi;

class Flashcard : public Layer{
	public:
		static Scene* createScene();
		virtual bool init();
		CREATE_FUNC(Flashcard);
		
		// fields
		Button* homeBtn = nullptr;

		void goToMainMenu();
		void createUI();
		void LoadData();
		void fadeInUI();

		// touch event handler
		virtual bool onTouchBegan(Touch* touch, Event* event);
		//virtual void onTouchMoved(Touch* touch, Event* event);
		virtual void onTouchEnded(Touch* touch, Event* event);
		virtual void onTouchCancelled(Touch* touch, Event* event);

	private:

		pugi::xml_document xmlDocument;

		IndexCard* indexCard1;
		IndexCard* indexCard2;
		const IndexCard* currentIndexCard; 
		GlobalConfig::DataClass indexCardDataClass;

		std::vector<pugi::xml_node> dataVec;

		int currentDataVecIndex = 0;
		Vec2 initTouchPosition;
		//Vec2 currentTouchPosition;

		void nextIndexCard(GlobalConfig::SwipeDirection swipeDirection);
};

#endif // !__FLASHCARD_H__

