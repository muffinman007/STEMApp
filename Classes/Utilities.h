#ifndef __UTILITIES_H__
#define __UTILITIES_H__

#include"cocos2d.h"
#include"GlobalConfig.h"
#include"ui\CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class Utilities : public Layer{
	public:
		static Scene* createScene();
		virtual bool init();
		CREATE_FUNC(Utilities);

		void goToMainMenu();

	private:
		Button* homeBtn;

};

#endif // !__UTILITIES_H__

