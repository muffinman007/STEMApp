#ifndef __MAINMENU_H__
#define __MAINMENU_H__

#include"cocos2d.h"
#include"ui\CocosGUI.h"
#include"GlobalConfig.h"
#include<string>

USING_NS_CC;
using namespace cocos2d::ui;
using namespace std;

class MainMenu : public Layer{
	public:
		static Scene* createScene();
		virtual bool init();
		CREATE_FUNC(MainMenu);

		Size size;
		GlobalConfig::Topic userSelectedTopic;
		bool wasHomeBtnPressed = false;

		Sprite * titleSprite = nullptr;		

		Sprite* leftHorBarSprt;
		Sprite* rightHorBarSprt;
		Sprite* verticalBarSprt;

		/*MenuItemLabel* firstMenuItem;
		MenuItemLabel* secondMenuItem;
		MenuItemLabel* thirdMenuItem;*/

		Button* firstBtn;    // left
		Button* secondBtn;   // right
		Button* thirdBtn;    // botttom one
		Button* homeBtn = nullptr;
		//Menu* mainMenuSelection;	


		// methods
		void userSelectionAction(GlobalConfig::Selection selection);
		void homeBtnPressedAction();
		
		void createMainMenuUI();
		void createSubMenuUI();
		void mainMenuSelectAction(Node* selectedNode);
		void startNewScene();		

	private:
		void FadeOutAction(Node* node, float duration, bool removeSelf);
		void setVisualSupportPos();

		// fields	
		GlobalConfig::Selection  userSelectedFeature;
};

#endif // !__MAINMENU_H__

