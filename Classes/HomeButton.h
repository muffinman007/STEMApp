#ifndef __HOMEBUTTON_H__
#define __HOMEBUTTON_H__

#include"cocos2d.h"
#include"MainMenu.h"

USING_NS_CC;

class HomeButton{
	public:
		static HomeButton& Instance(){
			static HomeButton myInstance;
			return myInstance;
		}

		// delete copy and move constructors and assign operators
		HomeButton(HomeButton const&)			 = delete;			// copy ctor
		HomeButton(HomeButton&&)				 = delete;			// Move ctor
		HomeButton& operator=(HomeButton const&) = delete;			// copy assign
		HomeButton& operator=(HomeButton&&)		 = delete;			// move assign

		Menu* getMenu(){ return homeMenu; };

		// fields
		static Menu* homeMenu;

	protected:
		HomeButton(){
			auto homeBtn = MenuItemLabel::create(Label::create("Home", "fonts/Marker Felt.ttf", 40), CC_CALLBACK_0(HomeButton::goToMainMenu, this));
			homeBtn->setPosition(Director::getInstance()->getVisibleSize().width * (7.0f/8.0f), Director::getInstance()->getVisibleSize().height * (7.0f/8.0f));
			homeMenu = Menu::create(homeBtn, nullptr);
			homeMenu->setPosition(Vec2::ZERO);
		}

		~HomeButton(){
		
		}	

		// method
		void goToMainMenu(){
			Director::getInstance()->replaceScene(MainMenu::createScene());
		}
};

#endif // !__HOMEBUTTON_H__

