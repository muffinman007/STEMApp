#include"GlobalConfig.h"

const float GlobalConfig::guiLeftXScalar		= 0.16f;
const float GlobalConfig::guiRightXScalar		= 0.84f;
const float GlobalConfig::guiTopYScalar			= 0.75f;
const float GlobalConfig::guiBottomYScalar		= 0.30f;

GlobalConfig::Topic GlobalConfig::topicSeleted;

const char GlobalConfig::xmlDelimitor			= '$';

const string GlobalConfig::xmlNodeBioQuizStr = "BioQuiz";
const string GlobalConfig::xmlNodeChemQuizStr = "ChemQuiz";
const string GlobalConfig::xmlBioQuizFileName = "data/biologyQuiz.xml";
const string GlobalConfig::xmlChemQuizFilename = "data/chemistryQuiz.xml";
const string GlobalConfig::xmlQuestionAttribute = "question";
const string GlobalConfig::xmlAnswerAttribute = "answer";
const string GlobalConfig::xmlAAttribute = "a";
const string GlobalConfig::xmlBAttribute = "b";
const string GlobalConfig::xmlCAttribute = "c";
const string GlobalConfig::xmlDAttribute = "d";


GlobalConfig::GlobalConfig(){
}