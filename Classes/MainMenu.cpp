#include"MainMenu.h"
#include"Flashcard.h"
#include"Quiz.h"
#include"Utilities.h"
#include"GlobalConfig.h"

Scene* MainMenu::createScene(){
	auto scene = Scene::create();
	auto layer = MainMenu::create();
	scene->addChild(layer);
	return scene;
}

bool MainMenu::init(){
	if(!Layer::init()) return false;

	// background sprite
	auto bg = LayerColor::create(Color4B(147, 140, 127, 255));
	bg->setAnchorPoint(Vec2::ZERO);
	bg->setPosition(Vec2::ZERO);
	this->addChild(bg, -1);

	size = Director::getInstance()->getVisibleSize();
	// Titles 
	titleSprite = Sprite::create("res/gui/guiSTEMLogo.png");
	titleSprite->setPosition(size.width / 2.0f, size.height * GlobalConfig::guiTopYScalar);

	this->addChild(titleSprite);

	// ui visual support
	// set position of the ui visual support after we placed the button on the screen. this help with the calculation
	leftHorBarSprt = Sprite::create("res/gui/guiHorizontalBar.png");
	rightHorBarSprt = Sprite::create("res/gui/guiHorizontalBar.png");
	verticalBarSprt = Sprite::create("res/gui/guiVerticalBar.png");
	
	createMainMenuUI();

	return true;
}

void MainMenu::createMainMenuUI(){
	firstBtn	= Button::create("res/gui/guiFlashcard.png", "res/gui/guiBlankPolygon.png");
	firstBtn->setPosition(Vec2(size.width * GlobalConfig::guiLeftXScalar, size.height * GlobalConfig::guiTopYScalar));
	firstBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					leftHorBarSprt->setColor(Color3B::RED);
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::userSelectionAction, this, GlobalConfig::Selection::FLASHCARD)),
						CallFunc::create(CC_CALLBACK_0(MainMenu::mainMenuSelectAction, this, dynamic_cast<Node*>(sender))),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					leftHorBarSprt->setColor(Color3B::WHITE);
					break;
			}
		}	
	);
	firstBtn->setSwallowTouches(true);

	secondBtn	= Button::create("res/gui/guiQuiz.png", "res/gui/guiBlankPolygon.png");
	secondBtn->setPosition(Vec2(size.width * GlobalConfig::guiRightXScalar, size.height * GlobalConfig::guiTopYScalar));
	secondBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					rightHorBarSprt->setColor(Color3B::RED);
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::userSelectionAction, this, GlobalConfig::Selection::QUIZ)),
						CallFunc::create(CC_CALLBACK_0(MainMenu::mainMenuSelectAction, this, dynamic_cast<Node*>(sender))),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					rightHorBarSprt->setColor(Color3B::WHITE);
					break;
			}
		}
	);
	secondBtn->setSwallowTouches(true);

	thirdBtn	= Button::create("res/gui/guiUtilities.png", "res/gui/guiBlankPolygon.png");
	thirdBtn->setPosition(Vec2(size.width / 2.0f, size.height * GlobalConfig::guiBottomYScalar));
	thirdBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					verticalBarSprt->setColor(Color3B::RED);
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::userSelectionAction, this, GlobalConfig::Selection::UTILITIES)),
						CallFunc::create(CC_CALLBACK_0(MainMenu::mainMenuSelectAction, this, dynamic_cast<Node*>(sender))),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					verticalBarSprt->setColor(Color3B::WHITE);
					break;
			}
		}
	);
	thirdBtn->setSwallowTouches(true);		

	setVisualSupportPos();

	if(wasHomeBtnPressed){
		// set opacity to 0 and fade in
		firstBtn->setOpacity(0);
		secondBtn->setOpacity(0);
		thirdBtn->setOpacity(0);

		this->addChild(firstBtn);
		this->addChild(secondBtn);
		this->addChild(thirdBtn);

		firstBtn->runAction(FadeIn::create(0.3f));
		secondBtn->runAction(FadeIn::create(0.3f));
		thirdBtn->runAction(FadeIn::create(0.3f));

		// title fade in		
		titleSprite->runAction(FadeIn::create(0.3f));

		leftHorBarSprt->runAction(FadeIn::create(0.3f));
		rightHorBarSprt->runAction(FadeIn::create(0.3f));
		verticalBarSprt->runAction(FadeIn::create(0.3f));

		wasHomeBtnPressed = false;

		this->runAction(DelayTime::create(0.3f));
	}
	else{
		// this part is only called once
		this->addChild(firstBtn);
		this->addChild(secondBtn);
		this->addChild(thirdBtn);		

		this->addChild(leftHorBarSprt);
		this->addChild(rightHorBarSprt);
		this->addChild(verticalBarSprt);
	}
}

void MainMenu::setVisualSupportPos(){
	// ui visual support
	// set position of the ui visual support after we placed the button on the screen. this help with the calculation
	float halfDistance = ((titleSprite->getPositionX() - titleSprite->getContentSize().width / 2.0f) - (firstBtn->getPositionX() + firstBtn->getContentSize().width / 2.0f)) / 2.0f;
	leftHorBarSprt->setPosition(titleSprite->getPositionX() - (titleSprite->getContentSize().width / 2.0f) - halfDistance, titleSprite->getPositionY());

	halfDistance =  ((secondBtn->getPositionX() - secondBtn->getContentSize().width / 2.0f) - (titleSprite->getPositionX() + titleSprite->getContentSize().width / 2.0f)) / 2.0f;
	rightHorBarSprt->setPosition(titleSprite->getPositionX() + (titleSprite->getContentSize().width / 2.0f) + halfDistance, titleSprite->getPositionY());

	halfDistance = ((titleSprite->getPositionY() - titleSprite->getContentSize().height / 2.0f) - (thirdBtn->getPositionY() + thirdBtn->getContentSize().height / 2.0f)) / 2.0f;
	verticalBarSprt->setPosition(titleSprite->getPositionX(), titleSprite->getPositionY() - (titleSprite->getContentSize().height / 2.0f) - halfDistance);
}

void MainMenu::userSelectionAction(GlobalConfig::Selection selection){
	this->userSelectedFeature = selection;
	if(selection == GlobalConfig::Selection::FLASHCARD){		
		FadeOutAction(secondBtn, 0.3f, true);
		FadeOutAction(thirdBtn, 0.3f, true);
		FadeOutAction(titleSprite, 0.3f, false);

		leftHorBarSprt->setColor(Color3B::WHITE);

		titleSprite->setTexture("res/gui/guiFlashcardLogo.png");
		titleSprite->setOpacity(0);
	}
	else if(selection == GlobalConfig::Selection::QUIZ){
		FadeOutAction(firstBtn, 0.3f, true);
		FadeOutAction(thirdBtn, 0.3f, true);
		FadeOutAction(titleSprite, 0.3f, false);

		rightHorBarSprt->setColor(Color3B::WHITE);
	
		titleSprite->setTexture("res/gui/guiQuizLogo.png");
		titleSprite->setOpacity(0);
	}
	else if(selection == GlobalConfig::Selection::UTILITIES){
		FadeOutAction(firstBtn, 0.3f, true);
		FadeOutAction(secondBtn, 0.3f, true);
		FadeOutAction(titleSprite, 0.3f, false);

		verticalBarSprt->setColor(Color3B::WHITE);

		titleSprite->setTexture("res/gui/guiUtilitiesLogo.png");
		titleSprite->setOpacity(0);
	}

	FadeOutAction(leftHorBarSprt, 0.3f, false);
	FadeOutAction(rightHorBarSprt, 0.3f, false);
	FadeOutAction(verticalBarSprt, 0.3f, false);

	this->runAction(DelayTime::create(0.3f));
}


void MainMenu::FadeOutAction(Node* node, float duration, bool removeSelf){
	node->runAction(Sequence::create(FadeOut::create(duration), removeSelf ? RemoveSelf::create(true), nullptr : nullptr));
}


void MainMenu::mainMenuSelectAction(Node* selectedNode){
	// move the menu offscreen and create the new subset of selection
	selectedNode->runAction(Sequence::create(
		DelayTime::create(0.2f),
		MoveBy::create(0.3f, Vec2(-size.width, 0.0f)),
		RemoveSelf::create(true),
		CallFunc::create(CC_CALLBACK_0(MainMenu::createSubMenuUI, this)),			
		nullptr
		)
	);
}


void MainMenu::createSubMenuUI(){
	if(userSelectedFeature == GlobalConfig::Selection::FLASHCARD){
		firstBtn	= Button::create("res/gui/normalFcChemistry.png",	"res/gui/selectFcChemistry.png");
		secondBtn	= Button::create("res/gui/normalFcBiology.png",		"res/gui/selectFcBiology.png");
		thirdBtn	= Button::create("res/gui/normalFcPolymath.png",	"res/gui/selectFcPolymath.png");

		//currentTitleSprt = 
	}
	else if(userSelectedFeature == GlobalConfig::Selection::QUIZ){
		firstBtn	= Button::create("res/gui/normalQChemistry.png",	"res/gui/selectQChemistry.png");
		secondBtn	= Button::create("res/gui/normalQBiology.png",		"res/gui/selectQBiology.png");
		thirdBtn	= Button::create("res/gui/normalQPolymath.png",		"res/gui/selectQPolymath.png");
	}
	else if(userSelectedFeature == GlobalConfig::Selection::UTILITIES){
		firstBtn	= Button::create("res/gui/normalUChemistry.png",	"res/gui/selectUChemistry.png");
		secondBtn	= Button::create("res/gui/normalUBiology.png",		"res/gui/selectUBiology.png");
		thirdBtn	= Button::create("res/gui/normalUPolymath.png",		"res/gui/selectUPolymath.png");
	}	

	firstBtn->setOpacity(0);
	firstBtn->setPosition(Vec2(size.width * 0.25f, size.height / 2.0f));
	firstBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					userSelectedTopic = GlobalConfig::Topic::CHEMISTRY;
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::startNewScene, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);

	secondBtn->setOpacity(0);
	secondBtn->setPosition(Vec2(size.width * 0.5f, size.height / 2.0f));
	secondBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					userSelectedTopic = GlobalConfig::Topic::BIOLOGY;
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::startNewScene, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);

	thirdBtn->setOpacity(0);
	thirdBtn->setPosition(Vec2(size.width * 0.75f, size.height / 2.0f));
	thirdBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					userSelectedTopic = GlobalConfig::Topic::POLYMATH;
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(MainMenu::startNewScene, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);

	
	if(homeBtn == nullptr){
		homeBtn = Button::create("res/gui/home.png",	"res/gui/homeSelect.png");
		homeBtn->setOpacity(0);
		homeBtn->setPosition(Vec2(size.width * 0.9f, size.height * 0.9f));
		homeBtn->addTouchEventListener(
			[&](Ref* sender, Widget::TouchEventType type){
				switch(type){
					case Widget::TouchEventType::BEGAN:
						dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
						break;
					case Widget::TouchEventType::MOVED:
						break;
					case Widget::TouchEventType::ENDED:
						dynamic_cast<Node*>(sender)->runAction(Sequence::create(
							ScaleTo::create(0.0f, 1.0f),
							Blink::create(0.2f, 3),
							CallFunc::create(CC_CALLBACK_0(MainMenu::homeBtnPressedAction, this)),
							CallFunc::create(CC_CALLBACK_0(MainMenu::createMainMenuUI, this)),
							nullptr
						));
						break;
					case Widget::TouchEventType::CANCELED:
						dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
						break;
				}
			}
		);
		homeBtn->setSwallowTouches(true);

		this->addChild(homeBtn);
	}

	this->addChild(firstBtn);
	this->addChild(secondBtn);
	this->addChild(thirdBtn);	
	
	firstBtn->runAction(FadeIn::create(1.0f));
	secondBtn->runAction(FadeIn::create(1.0f));
	thirdBtn->runAction(FadeIn::create(1.0f));
	homeBtn->runAction(FadeIn::create(1.2f));
	titleSprite->runAction(FadeIn::create(1.0f));	

	this->runAction(DelayTime::create(1.0f));
}

void MainMenu::homeBtnPressedAction(){
	wasHomeBtnPressed = true;
	// fade out the current button on screen and delete all icon and label.
	FadeOutAction(firstBtn, 0.2f, true);
	FadeOutAction(secondBtn, 0.2f, true);
	FadeOutAction(thirdBtn, 0.2f, true);
	FadeOutAction(titleSprite, 0.2f, false);
	FadeOutAction(homeBtn, 0.2f, false);

	this->runAction(DelayTime::create(0.25f));

	titleSprite->setTexture("res/gui/guiSTEMLogo.png");
}

void MainMenu::startNewScene(){
	GlobalConfig::topicSeleted = userSelectedTopic;
		 if(userSelectedFeature == GlobalConfig::Selection::FLASHCARD)	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.6f, Flashcard::createScene()));
	else if(userSelectedFeature == GlobalConfig::Selection::QUIZ)		Director::getInstance()->replaceScene(TransitionCrossFade::create(0.6f, Quiz::createScene()));
	else if(userSelectedFeature == GlobalConfig::Selection::UTILITIES)	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.6f, Utilities::createScene()));
}