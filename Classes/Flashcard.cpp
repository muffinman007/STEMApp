#include"Flashcard.h"
#include"MainMenu.h"
#include<cmath>
#include<algorithm>
#include<chrono>
#include<random>

#define TODO(x) //x

Scene* Flashcard::createScene(){
	auto scene = Scene::create();
	auto layer = Flashcard::create();
	scene->addChild(layer);
	
	return scene;
}

bool Flashcard::init(){
	if(!Layer::init()) return false;

	LoadData();
	createUI();
	
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan		= CC_CALLBACK_2(Flashcard::onTouchBegan,	 this);
	//touchListener->onTouchMoved		= CC_CALLBACK_2(Flashcard::onTouchMoved,	 this);
	touchListener->onTouchEnded		= CC_CALLBACK_2(Flashcard::onTouchEnded,	 this);
	touchListener->onTouchCancelled	= CC_CALLBACK_2(Flashcard::onTouchCancelled, this);

	fadeInUI();

	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);		

	return true;
}


void Flashcard::LoadData(){
	// loading data
	if(GlobalConfig::topicSeleted == GlobalConfig::Topic::CHEMISTRY){
		ssize_t size;
		char* rawData = (char*)FileUtils::getInstance()->getFileData("data/flashcardChem.xml", "r", &size);

		//string xmlPath = FileUtils::getInstance()->fullPathFromRelativeFile("flashcardChem.xml", "/data/");
		xml_parse_result xmlResult = xmlDocument.load_string(rawData);
		
		CC_SAFE_DELETE(rawData);

		if(xmlResult.status != xml_parse_status::status_ok){
			// error message.
			auto label1 = Label::createWithTTF("I/O Error", "fonts/Helvetica.ttf", 200);
			label1->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, Director::getInstance()->getVisibleSize().height * 0.9f);
			label1->setTextColor(Color4B::RED);
			this->addChild(label1);
		}
	
		xml_node parentNode = xmlDocument.child("PeriodicTable");
		for(xml_node node = parentNode.first_child(); node ; node = node.next_sibling()){
			dataVec.push_back(node);
		}	
	}
	else if(GlobalConfig::topicSeleted == GlobalConfig::Topic::BIOLOGY){
	
	}
	else if(GlobalConfig::topicSeleted == GlobalConfig::Topic::POLYMATH){
	
	}

	std::shuffle(std::begin(dataVec), std::end(dataVec), std::default_random_engine(chrono::system_clock::now().time_since_epoch().count()));
}


void Flashcard::goToMainMenu(){
	this->_eventDispatcher->removeAllEventListeners();

	// clean up
	dataVec.clear();

	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.7f, MainMenu::createScene()));
}

void Flashcard::createUI(){
	homeBtn = Button::create("res/gui/home.png", "res/gui/homeSelect.png");
	homeBtn->setOpacity(0);
	homeBtn->setPosition(Vec2(Director::getInstance()->getVisibleSize().width * 0.9f, Director::getInstance()->getVisibleSize().height * 0.9f));
	homeBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Flashcard::goToMainMenu, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	homeBtn->setSwallowTouches(true);
	this->addChild(homeBtn);


	// create index card
 TODO("need to randomly set the indexCardDataClass when we have more data")
	indexCardDataClass = GlobalConfig::DataClass::PERIODIC_TABLE;

	currentDataVecIndex = RandomHelper::random_int(0, (int)dataVec.size() - 1);
	indexCard1 = IndexCard::create();
	indexCard1->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, Director::getInstance()->getVisibleSize().height / 2.0f);
	indexCard1->setOpacity(0);
	indexCard1->DataFeed(GlobalConfig::DataClass::PERIODIC_TABLE, dataVec.at(currentDataVecIndex));
	this->addChild(indexCard1);

	int temp;
	do{
		temp = RandomHelper::random_int(0, (int)dataVec.size() - 1);
	}
	while(temp == currentDataVecIndex);
	currentDataVecIndex = temp;

	indexCard2 = IndexCard::create();
	indexCard2->setVisible(false);
	indexCard2->DataFeed(GlobalConfig::DataClass::PERIODIC_TABLE, dataVec.at(currentDataVecIndex));
	this->addChild(indexCard2);

	currentIndexCard = indexCard1;
}

void Flashcard::fadeInUI(){
	indexCard1->runAction(FadeIn::create(2.0f));
	homeBtn->runAction(FadeIn::create(2.0f));
}


// touch event handler
bool Flashcard::onTouchBegan(Touch* touch, Event* event){
	initTouchPosition = touch->getLocation();
	//currentTouchPosition = touch->getLocation();
	return true;
}

//void Flashcard::onTouchMoved(Touch* touch, Event* event){
//	//currentTouchPosition = touch->getLocation();
//}

void Flashcard::onTouchEnded(Touch* touch, Event* event){
	// might be unnesscary but I don't know
	if(initTouchPosition.x == -1.0f) return;

	Vec2 currentTouchPosition = touch->getLocation();

	// the really interesting thing happends here
	// which direction did the user swipe?
	if(initTouchPosition.x - currentTouchPosition.x > GlobalConfig::minSwipeDistance){
		// swiped left
		nextIndexCard(GlobalConfig::SwipeDirection::LEFT);
	}
	else if(initTouchPosition.x - currentTouchPosition.x < -GlobalConfig::minSwipeDistance){
		// swiped right
		nextIndexCard(GlobalConfig::SwipeDirection::RIGHT);
	}
	else if(initTouchPosition.y - currentTouchPosition.y > GlobalConfig::minSwipeDistance){
		// swiped down
		nextIndexCard(GlobalConfig::SwipeDirection::DOWN);
	}
	else if(initTouchPosition.y - currentTouchPosition.y < -GlobalConfig::minSwipeDistance){
		// swiped up
		nextIndexCard(GlobalConfig::SwipeDirection::UP);
	}
	else{
		if(initTouchPosition == currentTouchPosition || initTouchPosition.distance(currentTouchPosition) < GlobalConfig::minTapDistance){
			const_cast<IndexCard*>(currentIndexCard)->flip();
		}
	}

}

void Flashcard::onTouchCancelled(Touch* touch, Event* event){
	initTouchPosition.setPoint(-1.0f, -1.0f);
	//currentTouchPosition.setPoint(-1.0f, -1.0f);
}

void Flashcard::nextIndexCard(GlobalConfig::SwipeDirection swipeDirection){
TODO("In the future, when more data randomly select new indexCardDataClass value before feeding data")

	if(currentIndexCard == indexCard1){
		// feed second index card new data
		int temp;
		do{
			temp = RandomHelper::random_int(0, (int)dataVec.size() - 1);
		}
		while(temp == currentDataVecIndex);
		currentDataVecIndex = temp;
		if(indexCardDataClass == GlobalConfig::DataClass::PERIODIC_TABLE){
			indexCard2->DataFeed(indexCardDataClass, dataVec[currentDataVecIndex]);
		}
		
		if(swipeDirection == GlobalConfig::SwipeDirection::LEFT){
			indexCard1->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(-Director::getInstance()->getVisibleSize().width, 0.0f)), CallFunc::create([&](){ indexCard1->setVisible(false); }) , nullptr), nullptr));
		
			indexCard2->setPosition(Director::getInstance()->getVisibleSize().width + indexCard2->getContentSize().width / 2.0f, Director::getInstance()->getVisibleSize().height / 2.0f);
			indexCard2->setOpacity(0);
			indexCard2->setVisible(true);
			indexCard2->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(indexCard2->getContentSize().width / 2.0f - Director::getInstance()->getVisibleSize().width, 0.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::RIGHT){
			indexCard1->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(+Director::getInstance()->getVisibleSize().width, 0.0f)), CallFunc::create([&](){ indexCard1->setVisible(false); }) , nullptr), nullptr));
		
			indexCard2->setPosition(-indexCard2->getContentSize().width / 2.0f, Director::getInstance()->getVisibleSize().height / 2.0f);
			indexCard2->setOpacity(0);
			indexCard2->setVisible(true);
			indexCard2->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(Director::getInstance()->getVisibleSize().width - indexCard2->getContentSize().width / 2.0f, 0.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::UP){
			indexCard1->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(0.0f, Director::getInstance()->getVisibleSize().height)), CallFunc::create([&](){ indexCard1->setVisible(false); }) , nullptr), nullptr));
		
			indexCard2->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, -indexCard2->getContentSize().height / 2.0f);
			indexCard2->setOpacity(0);
			indexCard2->setVisible(true);
			indexCard2->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(0.0f, (Director::getInstance()->getVisibleSize().height + indexCard2->getContentSize().height) / 2.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::DOWN){
			indexCard1->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(0.0f, -Director::getInstance()->getVisibleSize().height)), CallFunc::create([&](){ indexCard1->setVisible(false); }) , nullptr), nullptr));
		
			indexCard2->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, Director::getInstance()->getVisibleSize().height + indexCard2->getContentSize().height / 2.0f);
			indexCard2->setOpacity(0);
			indexCard2->setVisible(true);
			indexCard2->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(0.0f, -(Director::getInstance()->getVisibleSize().height + indexCard2->getContentSize().height) / 2.0f)), nullptr));
		}		

		currentIndexCard = indexCard2;
	}
	else if(currentIndexCard == indexCard2){
		// feed first index card new data
		int temp;
		do{
			temp = RandomHelper::random_int(0, (int)dataVec.size() - 1);
		}
		while(temp == currentDataVecIndex);
		currentDataVecIndex = temp;
		if(indexCardDataClass == GlobalConfig::DataClass::PERIODIC_TABLE){
			indexCard1->DataFeed(indexCardDataClass, dataVec[currentDataVecIndex]);
		}

		
		if(swipeDirection == GlobalConfig::SwipeDirection::LEFT){
			indexCard2->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(-Director::getInstance()->getVisibleSize().width, 0.0f)), CallFunc::create([&](){ indexCard2->setVisible(false); }) , nullptr), nullptr));
		
			indexCard1->setPosition(Director::getInstance()->getVisibleSize().width + indexCard1->getContentSize().width / 2.0f, Director::getInstance()->getVisibleSize().height / 2.0f);
			indexCard1->setOpacity(0);
			indexCard1->setVisible(true);
			indexCard1->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(indexCard1->getContentSize().width / 2.0f - Director::getInstance()->getVisibleSize().width, 0.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::RIGHT){
			indexCard2->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(+Director::getInstance()->getVisibleSize().width, 0.0f)), CallFunc::create([&](){ indexCard2->setVisible(false); }) , nullptr), nullptr));
		
			indexCard1->setPosition(-indexCard1->getContentSize().width / 2.0f, Director::getInstance()->getVisibleSize().height / 2.0f);
			indexCard1->setOpacity(0);
			indexCard1->setVisible(true);
			indexCard1->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(Director::getInstance()->getVisibleSize().width - indexCard1->getContentSize().width / 2.0f, 0.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::UP){
			indexCard2->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(0.0f, Director::getInstance()->getVisibleSize().height)), CallFunc::create([&](){ indexCard2->setVisible(false); }) , nullptr), nullptr));
		
			indexCard1->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, -indexCard1->getContentSize().height / 2.0f);
			indexCard1->setOpacity(0);
			indexCard1->setVisible(true);
			indexCard1->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(0.0f, (Director::getInstance()->getVisibleSize().height + indexCard1->getContentSize().height) / 2.0f)), nullptr));
		}
		else if(swipeDirection == GlobalConfig::SwipeDirection::DOWN){
			indexCard2->runAction(Spawn::create(FadeOut::create(0.6f), Sequence::create(MoveBy::create(0.3f, Vec2(0.0f, -Director::getInstance()->getVisibleSize().height)), CallFunc::create([&](){ indexCard2->setVisible(false); }) , nullptr), nullptr));
		
			indexCard1->setPosition(Director::getInstance()->getVisibleSize().width / 2.0f, Director::getInstance()->getVisibleSize().height + indexCard1->getContentSize().height / 2.0f);
			indexCard1->setOpacity(0);
			indexCard1->setVisible(true);
			indexCard1->runAction(Spawn::create(FadeIn::create(0.6f), MoveBy::create(0.3f, Vec2(0.0f, -(Director::getInstance()->getVisibleSize().height + indexCard1->getContentSize().height) / 2.0f)), nullptr));
		}
		

		currentIndexCard = indexCard1;
	}
}