#ifndef __INDEXCARD_H__
#define	__INDEXCARD_H__

#include"cocos2d.h"
#include"GlobalConfig.h"
#include<string>
#include"pugixml.hpp"

USING_NS_CC;
using namespace std;
using namespace pugi;

class IndexCard : public Sprite{
	public:
		static IndexCard* create();	

		void DataFeed(GlobalConfig::DataClass dataClass, xml_node data);
		void flip();

		Label* chemSymbol	= nullptr;
		Label* chemAtomNum	= nullptr;
		Label* chemName		= nullptr;
		Label* chemAtomMass	= nullptr;

	private:
		bool isAnswerShown;

		bool chemSymbolVisible		= false;
		bool chemAtomNumVisible		= false;	
		bool chemNameVisible		= false;		
		bool chemAtomMassVisible	= false;	

};

#endif // !__INDEXCARD_H__

