#ifndef __GLOBALCONFIG_H__
#define __GLOBALCONFIG_H__

#include<string>

using namespace std;

class GlobalConfig{
	public:
		GlobalConfig();

		enum class Selection{
			FLASHCARD,
			QUIZ,
			UTILITIES
		};

		enum class Topic{
			CHEMISTRY,
			BIOLOGY,
			POLYMATH
		};

		enum class DataClass{
			PERIODIC_TABLE
		};

		enum class SwipeDirection{
			LEFT,
			RIGHT,
			UP,
			DOWN
		};

		const static int minSwipeDistance = 64;
		const static int minTapDistance = 5;

		const static float guiLeftXScalar;
		const static float guiRightXScalar;
		const static float guiTopYScalar;
		const static float guiBottomYScalar;

		const static char xmlDelimitor;

		static Topic topicSeleted;

		const static string xmlNodeBioQuizStr	;	
		const static string xmlNodeChemQuizStr	;	
		const static string xmlBioQuizFileName	;	
		const static string xmlChemQuizFilename	;	
		const static string xmlQuestionAttribute;	
		const static string xmlAnswerAttribute	;	
		const static string xmlAAttribute		;	
		const static string xmlBAttribute		;	
		const static string xmlCAttribute		;	
		const static string xmlDAttribute		;	
												
};


#endif // !__GLOBALCONFIG_H__

