#include"Quiz.h"
#include"MainMenu.h"
#include<algorithm>
#include<string>
#include<iostream>
#include<sstream>

using namespace std;

float Quiz::answerFontSize	 = 40.0f;
float Quiz::questionFontSize = 60.0f;
float Quiz::answerLabelXOffset	 = 20.0f;

Scene* Quiz::createScene(){
	auto scene = Scene::create();
	auto layer = Quiz::create();
	scene->addChild(layer);
	
	return scene;
}

bool Quiz::init(){
	if(!Layer::init()) return false;

	loadData();
	createUI();
	nextQuestion();

	// UI
	homeBtn = Button::create("res/gui/home.png", "res/gui/homeSelect.png");
	homeBtn->setOpacity(0);
	homeBtn->setPosition(Vec2(Director::getInstance()->getVisibleSize().width * 0.9f, Director::getInstance()->getVisibleSize().height * 0.9f));
	homeBtn->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Quiz::goToMainMenu, this)),
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	homeBtn->setSwallowTouches(true);
	this->addChild(homeBtn);


	homeBtn->runAction(FadeIn::create(2.0f));
	question->runAction(FadeIn::create(2.0f));
	answerA->runAction(FadeIn::create(2.0f));
	answerB->runAction(FadeIn::create(2.0f));
	answerC->runAction(FadeIn::create(2.0f));
	answerD->runAction(FadeIn::create(2.0f));


	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan		= CC_CALLBACK_2(Quiz::onTouchBegin,	 this);
	touchListener->onTouchMoved		= CC_CALLBACK_2(Quiz::onTouchMoved,	this);
	touchListener->onTouchEnded		= CC_CALLBACK_2(Quiz::onTouchEnded,	 this);
	touchListener->onTouchCancelled	= CC_CALLBACK_2(Quiz::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);		
		
	return true;
}

void Quiz::goToMainMenu(){
	this->_eventDispatcher->removeAllEventListeners();

	// clean up
	dataVec.clear();

	Director::getInstance()->replaceScene(TransitionCrossFade::create(0.7f, MainMenu::createScene()));
}


void Quiz::loadData(){
	if(GlobalConfig::topicSeleted == GlobalConfig::Topic::BIOLOGY){
		extractXmlBioDoc();
	}
	else if(GlobalConfig::topicSeleted == GlobalConfig::Topic::CHEMISTRY){
		extractXmlChemDoc();
	}
	else if(GlobalConfig::topicSeleted == GlobalConfig::Topic::POLYMATH){
		extractXmlBioDoc();
		extractXmlChemDoc();
	}
	std::shuffle(std::begin(dataVec), std::end(dataVec), std::default_random_engine(chrono::system_clock::now().time_since_epoch().count()));
}

void Quiz::extractXmlBioDoc(){
	ssize_t size;
	char* rawData = (char*)FileUtils::getInstance()->getFileData(GlobalConfig::xmlBioQuizFileName.c_str(), "r", &size);
				
	xml_parse_result xmlResult = xmlBioDoc.load_string(rawData);

	CC_SAFE_DELETE(rawData);
	
	xml_node parentNode = xmlBioDoc.child(GlobalConfig::xmlNodeBioQuizStr.c_str());
	for(xml_node node = parentNode.first_child(); node; node = node.next_sibling()){
		dataVec.push_back(node);
	}
}

void Quiz::extractXmlChemDoc(){
	ssize_t size;
	char* rawData = (char*)FileUtils::getInstance()->getFileData(GlobalConfig::xmlChemQuizFilename.c_str(), "r", &size);
				
	xml_parse_result xmlResult = xmlChemDoc.load_string(rawData);

	CC_SAFE_DELETE(rawData);
	
	xml_node parentNode = xmlChemDoc.child(GlobalConfig::xmlNodeChemQuizStr.c_str());
	for(xml_node node = parentNode.first_child(); node; node = node.next_sibling()){
		dataVec.push_back(node);
	}
}

void Quiz::createUI(){
	float yOffset = Director::getInstance()->getVisibleSize().height / 8.0f;
	float xOffset = Director::getInstance()->getVisibleSize().width / 4.0f;

	float yScalar = 4.0f;
	float xScalar = 3.0f;

	question = Label::createWithTTF("", "fonts/Marker Felt.ttf", questionFontSize);
	question->setOpacity(0);
	question->setPosition(50.0f, Director::getInstance()->getVisibleSize().height * 0.95f);
	question->setAnchorPoint(Vec2(0.0f, 1.0f));
	

	answerA = Button::create("res/gui/quizButton.png");
	answerA->setOpacity(0);
	answerA->setPosition(Vec2(xOffset, yOffset * yScalar));
	answerA->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Quiz::checkAnswer, this, AnswerSelection::A)),						
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	answerA->setSwallowTouches(true);

	aLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", answerFontSize);
	aLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	aLabel->setPosition(Vec2(answerLabelXOffset,  answerA->getContentSize().height / 2.0f));
	answerA->addChild(aLabel);


	answerB = Button::create("res/gui/quizButton.png");
	answerB->setOpacity(0);
	answerB->setPosition(Vec2(xOffset * xScalar, yOffset * yScalar));
	answerB->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Quiz::checkAnswer, this, AnswerSelection::B)),						
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	answerB->setSwallowTouches(true);

	bLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", answerFontSize);
	bLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	bLabel->setPosition(Vec2(answerLabelXOffset,  answerB->getContentSize().height / 2.0f));
	answerB->addChild(bLabel);


	answerC = Button::create("res/gui/quizButton.png");
	answerC->setOpacity(0);
	answerC->setPosition(Vec2(xOffset, yOffset));
	answerC->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Quiz::checkAnswer, this, AnswerSelection::C)),						
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	answerC->setSwallowTouches(true);

	cLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", answerFontSize);
	cLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	cLabel->setPosition(Vec2(answerLabelXOffset,  answerC->getContentSize().height / 2.0f));
	answerC->addChild(cLabel);


	answerD = Button::create("res/gui/quizButton.png");
	answerD->setOpacity(0);
	answerD->setPosition(Vec2(xOffset * xScalar, yOffset));
	answerD->addTouchEventListener(
		[&](Ref* sender, Widget::TouchEventType type){
			switch(type){
				case Widget::TouchEventType::BEGAN:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.1f));
					break;
				case Widget::TouchEventType::MOVED:
					break;
				case Widget::TouchEventType::ENDED:
					dynamic_cast<Node*>(sender)->runAction(Sequence::create(
						ScaleTo::create(0.0f, 1.0f),
						Blink::create(0.2f, 3),
						CallFunc::create(CC_CALLBACK_0(Quiz::checkAnswer, this, AnswerSelection::D)),						
						nullptr
					));
					break;
				case Widget::TouchEventType::CANCELED:
					dynamic_cast<Node*>(sender)->runAction(ScaleTo::create(0.1f, 1.0f));
					break;
			}
		}
	);
	answerD->setSwallowTouches(true);

	dLabel = Label::createWithTTF("", "fonts/Marker Felt.ttf", answerFontSize);
	dLabel->setAnchorPoint(Vec2(0.0f, 0.5f));
	dLabel->setPosition(Vec2(answerLabelXOffset,  answerD->getContentSize().height / 2.0f));
	answerD->addChild(dLabel);
	
	question->setTextColor(Color4B::WHITE);
	aLabel->setTextColor(Color4B::BLACK);
	bLabel->setTextColor(Color4B::BLACK);
	cLabel->setTextColor(Color4B::BLACK);
	dLabel->setTextColor(Color4B::BLACK);


	this->addChild(question);
	this->addChild(answerA);
	this->addChild(answerB);
	this->addChild(answerC);
	this->addChild(answerD);
}

void Quiz::nextQuestion(){
	// reset color of all the button
	answerA->setColor(Color3B::WHITE);
	answerB->setColor(Color3B::WHITE);
	answerC->setColor(Color3B::WHITE);
	answerD->setColor(Color3B::WHITE);

	answerA->runAction(ScaleTo::create(0.0f, 1.0f));
	answerB->runAction(ScaleTo::create(0.0f, 1.0f));
	answerC->runAction(ScaleTo::create(0.0f, 1.0f));
	answerD->runAction(ScaleTo::create(0.0f, 1.0f));

	// fade out all the label
	question->runAction(FadeOut::create(0.2f));
	aLabel->runAction(FadeOut::create(0.2f));
	bLabel->runAction(FadeOut::create(0.2f));
	cLabel->runAction(FadeOut::create(0.2f));
	dLabel->runAction(FadeOut::create(0.2f));

	this->runAction(DelayTime::create(0.2f));

	// extract the data 	
	xml_node parentNode = dataVec.at(currentIndex);


	// for the question label if the string is too long split them into two lines.
	string questionStr = getString(parentNode.attribute(GlobalConfig::xmlQuestionAttribute.c_str()).value());
	if(questionStr.length() <= 60){
		question->setString(questionStr);
	}
	else if(questionStr.length() <= 120){
		ostringstream ss;	
		bool addNewLine = false;
		for(int i = 0; i < questionStr.length(); ++i){
			ss << questionStr[i];
			// look for the first space char between the 50th and 60th letters
			if(i >= 49 && i <= 59){
				if(questionStr[i] == ' ' && !addNewLine){
					ss << "\n";
					addNewLine = true;
				}
			}			
		}

		question->setString(ss.str());
	}
	else{
		// over 120 chars.. question might be too long
		ostringstream ss;	
		bool add1stNewLine = false;
		bool add2ndNewLine = false;
		for(int i = 0; i < questionStr.length(); ++i){
			ss << questionStr[i];
			// look for the first space char between the 50th and 60th letters
			if(i >= 49 && i <= 59){
				if(questionStr[i] == ' ' && !add1stNewLine){
					ss << "\n";
					add1stNewLine = true;
				}
			}
			else if(i >= 109 && i <= 119){
				if(questionStr[i] == ' ' && !add2ndNewLine){
					ss << "\n";
					add2ndNewLine = true;
				}
			}
		}

		question->setString(ss.str());
	}
	
	correctAnswer = parentNode.attribute(GlobalConfig::xmlAnswerAttribute.c_str()).as_int();

	xml_node childNode = parentNode.first_child();
	aLabel->setString(getString(childNode.attribute(GlobalConfig::xmlAAttribute.c_str()).value()));
	bLabel->setString(getString(childNode.attribute(GlobalConfig::xmlBAttribute.c_str()).value()));
	cLabel->setString(getString(childNode.attribute(GlobalConfig::xmlCAttribute.c_str()).value()));
	dLabel->setString(getString(childNode.attribute(GlobalConfig::xmlDAttribute.c_str()).value()));

	++currentIndex;
	if(currentIndex >= dataVec.size()){
		currentIndex = 0;
		std::shuffle(std::begin(dataVec), std::end(dataVec), std::default_random_engine(chrono::system_clock::now().time_since_epoch().count()));
	}


	question->runAction(FadeIn::create(0.2f));
	aLabel->runAction(FadeIn::create(0.2f));
	bLabel->runAction(FadeIn::create(0.2f));
	cLabel->runAction(FadeIn::create(0.2f));
	dLabel->runAction(FadeIn::create(0.2f));

	this->runAction(DelayTime::create(0.2f));
}

void Quiz::checkAnswer(AnswerSelection answerSelected){
	if(static_cast<int>(answerSelected) != correctAnswer){
		// red the wronge answer
		if(answerSelected == AnswerSelection::A){
			answerA->setColor(Color3B::RED);
		}
		else if(answerSelected == AnswerSelection::B){
			answerB->setColor(Color3B::RED);
		}
		else if(answerSelected == AnswerSelection::C){
			answerC->setColor(Color3B::RED);
		}
		else if(answerSelected == AnswerSelection::D){
			answerD->setColor(Color3B::RED);
		}
	}

	if(correctAnswer == 0){
		answerA->setColor(Color3B::GREEN);
	}
	else if(correctAnswer == 1){
		answerB->setColor(Color3B::GREEN);
	}
	else if(correctAnswer == 2){
		answerC->setColor(Color3B::GREEN);
	}
	else if(correctAnswer == 3){
		answerD->setColor(Color3B::GREEN);
	}
}


// touch event handler
bool Quiz::onTouchBegin(Touch* touch, Event* event){
	initTouchPosition = touch->getLocation();
	//currentTouchPosition = touch->getLocation();
	return true;
}

void Quiz::onTouchEnded(Touch* touch, Event* event){
	// might be unnesscary but I don't know
	if(initTouchPosition.x == -1.0f) return;

	Vec2 currentTouchPosition = touch->getLocation();

	// the really interesting thing happends here
	// which direction did the user swipe?
	if(initTouchPosition.x - currentTouchPosition.x > GlobalConfig::minSwipeDistance){
		// swiped left
		nextQuestion();
		
	}
	else if(initTouchPosition.x - currentTouchPosition.x < -GlobalConfig::minSwipeDistance){
		// swiped right
		nextQuestion();
	}
}

void Quiz::onTouchCancelled(Touch* touch, Event* event){
	initTouchPosition.setPoint(-1.0f, -1.0f);
	//currentTouchPosition.setPoint(-1.0f, -1.0f);
}

void Quiz::onTouchMoved(Touch* touch, Event* event){}

string Quiz::getString(string value){
	ostringstream ss;

	for(int i = 0; i < value.length(); ++i){
		if(value[i] == GlobalConfig::xmlDelimitor){
			ss << " ";
		}
		else{
			ss << value[i];
		}
	}

	return ss.str();
}